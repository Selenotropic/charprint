# CharPrint

CharPrint is a simple but flexible library intended for expressive text, such as digital poetry and text-based games. It can be used with curses windows, a bare console, or to output characters that can be incorporated into your pipeline.

**Note:** Currently in active development.

## Concepts
The core of CharPrint is the Printable class. This is an object that represents some text, a collection of associated settings, and, optionally, an associated curses window.

## API Reference
